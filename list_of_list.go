package grammemes

import (
	"fmt"
	"strings"
)

// ListList предоставляет контейнер для хранения списка наборов граммем.
type ListList []*List

// String возвращает строковое представления списка наборов граммем.
func (listOfList ListList) String() string {
	res := make([]string, 0)
	for idx, list := range listOfList {
		res = append(res, fmt.Sprintf("%d:%v", idx, list.String()))
	}

	return strings.Join(res, ",")
}
